package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"net"
)

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		var game_tick float64
		var game_id string
		if val, ok := msg["gameId"].(string); ok { game_id = val }
		if val, ok := msg["gameTick"].(float64); ok { game_tick = val }
		switch msg[c_data].(type) {
		default:
			err = dispatch_msg(writer, msg[c_msg_type].(string), nil, game_id, game_tick)
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg[c_msg_type].(string), msg[c_data].(interface{}), game_id, game_tick)
			if err != nil {
				return
			}
		}
	}
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m[c_msg_type] = msgtype
	m[c_data] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	send_join(writer, name, key)
	for {
		input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		err = parse_and_dispatch_input(writer, input)
		if err != nil {
			log_and_exit(err)
		}
	}
	return err
}
