package main

import (
	"log"
	"math"
)

const (
	c_max_desired_speed   = 30.00
	c_delta_desired_speed = 0.1
)

func f2i(a float64) (int) {
	return int(math.Floor(a + 0.5))
}

type Car struct {
	name,     color                      string
	length,     width,     flag_position float64
}

func (car *Car) compare(rival Car) (bool) {
	return car.name == rival.name && car.color == rival.color
}

type StartingPoint struct {
	x,     y,     angle float64
}

type Piece struct {
	length,     angle,     radius float64
	has_switch                    bool
}

func (piece *Piece) is_straight() (bool) {
	return piece.angle == 0.0 && piece.radius == 0.0
}

type Lane struct {
	distance,     index float64
}

type Track struct {
	id                 string
	name               string
	pieces             []Piece
	lanes              []Lane
	start              StartingPoint
	laps,     max_time float64
	quick              bool
}

type Position struct {
	angle                                          float64
	piece_index                                    int
	piece_distance                                 float64
	start_lane_index,     end_lane_index           int
	lap,     tick,     speed,     percent_distance float64
	first_on_this_piece                            bool
	speed_delta, road_delta, angle_delta float64
}

type AI struct {
	my_car       Car
	my_position  Position
	rivals       []Car
	positions    []Position
	track        Track
	switch_index int
	ready_turbo    bool
	last_throttle float64
}

func (ai *AI) is_my_car(car Car) (bool) {
	return ai.my_car.compare(car)
}

// piece manipulation methods

func (ai *AI) previous_index(index int) (prev_index int) {
	prev_index = index - 1
	if (prev_index < 0) {
		prev_index = len(ai.track.pieces) - 1
	}
	return
}

func (ai *AI) next_index(index int) (next_index int) {
	next_index = index + 1
	if (next_index >= len(ai.track.pieces)) {
		next_index = 0
	}
	return
}

func (ai *AI) next_speed_lowering() (speed, road float64) {
	road = ai.piece_length(ai.current_piece(), ai.my_position) - ai.my_position.piece_distance
	base_index := ai.my_position.piece_index
	_, base_speed := ai.desired_speed_for_piece(ai.current_piece())
	for i := 0; i < len(ai.track.pieces); i++ {
		base_index = ai.next_index(base_index)
		piece := ai.track.pieces[base_index]
		speed, _ = ai.desired_speed_for_piece(piece)
		//		log.Printf("index: %d, piece: %v, speed: %f, road: %f", base_index, piece, road, speed)
		if (speed < base_speed) {
			break
		} else {
			road = road + ai.piece_length(piece, ai.my_position)
		}
	}
	if (base_index == ai.my_position.piece_index || speed == base_speed) {
		road = 0.0
	}
	return
}

func (ai *AI)current_piece() (current Piece) {
	curr_index := ai.my_position.piece_index
	current = ai.track.pieces[curr_index]
	return
}

func (ai *AI) current_next_piece() (current, next, nexter Piece) {
	curr_index := ai.my_position.piece_index
	next_index := ai.next_index(curr_index)
	nexter_index := ai.next_index(next_index)
	current = ai.track.pieces[curr_index]
	next = ai.track.pieces[next_index]
	nexter = ai.track.pieces[nexter_index]
	return
}

func (ai *AI) piece_length(piece Piece, position Position) (length float64) {
	length = piece.length
	if (f2i(length) == 0) {
		var start_lane_radius, end_lane_radius float64
		if (piece.angle > 0) {
			start_lane_radius = piece.radius - ai.track.lanes[position.start_lane_index].distance
			end_lane_radius = piece.radius - ai.track.lanes[position.end_lane_index].distance
		} else {
			start_lane_radius = piece.radius + ai.track.lanes[position.start_lane_index].distance
			end_lane_radius = piece.radius + ai.track.lanes[position.end_lane_index].distance
		}
		factor := math.Pi*(math.Abs(piece.angle)/360.0)
		length = start_lane_radius*factor + end_lane_radius*factor
	}
	return
}

func (ai *AI) current_speed(old, new Position) (speed float64, first bool, road float64) {
	road = new.piece_distance
	tics := new.tick - old.tick
	first = old.piece_index != new.piece_index
	if (first) {
		current := ai.track.pieces[old.piece_index]
		length := ai.piece_length(current, old)
		road = road + (length - old.piece_distance)
	} else {
		road = road - old.piece_distance
		first = false
	}
	speed = road/tics
	if (speed < 0) {
		current := ai.track.pieces[old.piece_index]
		log.Printf("current: %v, ai: %v, current: %v", new, old, current)
	}
	return
}

func (ai *AI) percent_distance(position Position) (float64) {
	piece := ai.track.pieces[position.piece_index]
	return position.piece_distance/ai.piece_length(piece, position)*100.0
}

func (ai *AI) update_car_position(car Car, position Position) {
	if (ai.is_my_car(car)) {
		old_speed := ai.my_position.speed
		position.speed, position.first_on_this_piece, position.road_delta = ai.current_speed(ai.my_position, position)
		position.percent_distance = ai.percent_distance(position)
		position.speed_delta = position.speed - old_speed
		position.angle_delta = position.angle - ai.my_position.angle
		ai.my_position = position
	} else {
		for i := 0; i < len(ai.rivals); i++ {
			if (ai.rivals[i].compare(car)) {
				position.speed, position.first_on_this_piece, position.road_delta = ai.current_speed(ai.positions[i], position)
				position.percent_distance = ai.percent_distance(position)
				ai.positions[i] = position
			}
		}
	}
}

func (ai *AI) radius_from_piece(piece Piece) (start, end float64) {
	if (piece.angle == 0.0) {
		start = 0.0
		end = 0.0
	} else if (piece.angle > 0) {
		start = piece.radius - ai.track.lanes[ai.my_position.start_lane_index].distance
		end = piece.radius - ai.track.lanes[ai.my_position.end_lane_index].distance
	} else {
		start = piece.radius + ai.track.lanes[ai.my_position.start_lane_index].distance
		end = piece.radius + ai.track.lanes[ai.my_position.end_lane_index].distance
	}
	return
}

func (ai *AI) is_bend_in_coming_pieces(count int) (is bool) {
	curr_index := ai.my_position.piece_index
	for i := 0; i < count; i++ {
		next_index := ai.next_index(curr_index)
		if (!ai.track.pieces[next_index].is_straight()) {
			is = true
			break
		}
		curr_index = next_index
	}
	return
}

func (ai *AI) has_turbo(duration, tics, factor float64) {
	ai.ready_turbo = true
	log.Printf("turbo_received")
}

func (ai *AI) should_turbo() (should bool) { //  || ai.my_position.lap == ai.track.laps-1
	current, _, _ := ai.current_next_piece()
	road_clear := !ai.is_bend_in_coming_pieces(4) && current.is_straight() //|| ai.my_position.percent_distance < 50)
	//			ai.throttle() > 0.5 &&
	if (road_clear && ai.ready_turbo) {
		should = true
		ai.ready_turbo = false
		log.Printf("turbo_used")
	} else {
		should = false
	}
	return
}

func (ai *AI) should_switch_lane() (should bool) {
	current, next, nexter := ai.current_next_piece()
	current_index := ai.my_position.piece_index
	next_index := ai.next_index(ai.my_position.piece_index)
	nexter_index := ai.next_index(next_index)
	if (ai.switch_index != current_index && ai.switch_index != next_index && ai.switch_index != nexter_index) {

		if (current.has_switch && current.is_straight() && !next.is_straight()) {
			should = true
		} else if (next.has_switch && next.is_straight() && !nexter.is_straight()) {
			should = true
		}

		left := ai.switch_to_left()

		if (should) {
			lane_last_index := len(ai.track.lanes) - 1
			lane_index := ai.my_position.end_lane_index
			if (left && lane_index == 0) {
				should = false
			} else if (!left && lane_index == lane_last_index) {
				should = false
			}
		}

		if (should) {
			if (left) {
				log.Printf("switching to left")
			} else {
				log.Printf("switching to right")
			}
			ai.switch_index = ai.my_position.piece_index
		}
	}

	return
}

func (ai *AI) switch_to_left() (switch_to_left bool) {
	_, next, nexter := ai.current_next_piece()
	if (!next.is_straight()) {
		if (next.angle < 0) {
			switch_to_left = true
		} else {
			switch_to_left = false
		}
	} else if (!nexter.is_straight()) {
		if (nexter.angle < 0) {
			switch_to_left = true
		} else {
			switch_to_left = false
		}
	}
	return
}

func (ai *AI) desired_speed_for_piece(piece Piece) (speed_start, speed_end float64) {
	if (piece.is_straight()) {
		speed_start = c_max_desired_speed
		speed_end = c_max_desired_speed
	} else {
		start, end := ai.radius_from_piece(piece)
		speed_start = math.Sqrt(start)*c_t
		speed_end = math.Sqrt(end)*c_t
	}
	return
}

func (ai * AI) road_to_lower(speed float64, delta float64) (road float64) {
	acc := 0.0
	for i := 0; i < 10000; i++ {
		if (acc >= delta || speed <= 0.0) {
			return
		} else {
			road = road + speed
			acc = acc + (speed / 50.0)
			speed = speed - (speed / 50.0)
		}
	}
	return
}

func (ai *AI) throttle_for_speed(actual, desired float64) (throttle float64) {
	throttle = 5 * (desired - actual) + 0.1 * actual
	throttle = math.Min(math.Max(throttle, 0.0), 1.0)
	return
}

func (ai *AI) throttle() (throttle float64) {

	current, next, nexter := ai.current_next_piece()
	desired_speed := c_max_desired_speed //math.Inf(1)

	if (current.angle > 0.0 && ai.my_position.angle > 0.0) {
		radians := (2 * math.Pi * ai.my_position.angle) / 360.0

		ctg := 1.0 / math.Tan(radians)
		cos := math.Cos(radians)
		r1 := ai.my_car.length - ai.my_car.flag_position
		r2 := ai.my_car.flag_position
//		factor := ai.my_position.speed * ai.my_position.speed

		model_angle := ctg / r1 - cos / r2

		log.Printf("model angle: %f, angle: %f, radians %f, ctg %f, cos %f", model_angle, ai.my_position.angle_delta, radians, ctg, cos)
	}

	//	log.Printf("speed: %f, car angle: %f, delta angle: %f, model delta: %f, angle: %f, radius: %f", ai.my_position.speed, ai.my_position.angle, ai.my_position.angle_delta, model_angle, current.angle, radius)

	speed, road := ai.next_speed_lowering()
	delta := ai.my_position.speed - speed

	road_lower := ai.road_to_lower(ai.my_position.speed, delta)

	if (!current.is_straight() || !next.is_straight() || !nexter.is_straight()) {
		if (!current.is_straight() && ai.my_position.percent_distance < 40) {
			_, end := ai.radius_from_piece(current)
			desired_speed = math.Sqrt(end)*c_t
		}
		if (!next.is_straight()) {
			start, _ := ai.radius_from_piece(next)
			desired_speed = math.Sqrt(start)*c_t
		}
	}

	prev_speed := ai.my_position.speed - ai.my_position.speed_delta
	next_speed := prev_speed - prev_speed/50.0
	if (ai.last_throttle > 0.5) {
		next_speed = next_speed + 0.2 * ai.last_throttle
	}

	if (road == 0.0 || road < road_lower) {
		desired_speed = speed
	}

	//	if (math.Abs(ai.my_position.angle) > 55.0) {
	//		throttle = 0.0
	//	} else
	if (math.Abs(next_speed - ai.my_position.speed) > 1.0) {
		throttle = 1.0
	} else {//if (desired_speed - c_delta_desired_speed > ai.my_position.speed) {
		throttle = ai.throttle_for_speed(ai.my_position.speed, desired_speed)
	}

	ai.last_throttle = throttle

	//	log.Printf("delta %f, road %f, lower %f, should brake: %t, is breaking: %t", delta, road, road_lower, road < road_lower, throttle < 0.5)
	//	if (ai.my_position.speed_delta > 0) {
	//	log.Printf("speed current/desired: %f / %f, throttle: %f, angle: %f", ai.my_position.speed, desired_speed, throttle, ai.my_position.angle)
	//		prev_speed := ai.my_position.speed - ai.my_position.speed_delta
	//		log.Printf("speed: %f, computed delta: %f, real delta: %f, road delta: %f", prev_speed, 0.2 - prev_speed / 50.0, ai.my_position.speed_delta, ai.my_position.road_delta)
	//	}
	return
}

