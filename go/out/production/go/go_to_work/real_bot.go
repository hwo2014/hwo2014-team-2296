package main

import (
	"bufio"
	"log"
)

var ai AI

func parse_join(writer *bufio.Writer) (err error) {
	log.Printf("Joined")
	err = send_ping(writer)
	return
}

func parse_your_car(writer *bufio.Writer, data map[string]interface{}) (err error) {
	ai.my_car.name = data[c_name].(string)
	ai.my_car.color = data[c_color].(string)
	log.Printf("yourCar - name: %s, color: %s", ai.my_car.name, ai.my_car.color)
	err = send_ping(writer)
	return
}

func parse_turbo(writer *bufio.Writer, data map[string]interface{}) (err error) {
	duration := data[c_turbo_duration].(float64)
	tics := data[c_turbo_tics].(float64)
	factor := data[c_turbo_factor].(float64)
	ai.has_turbo(duration, tics, factor)
	if (ai.should_turbo()) {
		err = send_turbo(writer)
	} else {
		err = send_ping(writer)
	}
	return
}

func parse_game_init(writer *bufio.Writer, data map[string]interface{}) (err error) {

	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered in f", r)
		}
	}()

	log.Printf("gameInit")

	track_col := data["race"].(map[string]interface {})["track"].(map[string]interface {})

	pieces_col := track_col["pieces"].([]interface {}) // iterate oer them to get Pieces
	value := len(pieces_col)
	var pieces = make([]Piece, value)
	for i := 0; i < value; i++ {
		var length float64
		if _, ok := pieces_col[i].(map[string]interface {})["length"].(float64); ok {
			length = pieces_col[i].(map[string]interface {})["length"].(float64)
		}
		var radius float64
		if _, ok := pieces_col[i].(map[string]interface {})["radius"].(float64); ok {
			radius = pieces_col[i].(map[string]interface {})["radius"].(float64)
		}
		var angle float64
		if _, ok := pieces_col[i].(map[string]interface {})["angle"].(float64); ok {
			angle = pieces_col[i].(map[string]interface {})["angle"].(float64)
		}
		var switch_ bool
		if _, ok := pieces_col[i].(map[string]interface {})["switch"].(bool); ok {
			switch_ = pieces_col[i].(map[string]interface {})["switch"].(bool)
		}
		pieces[i] = Piece { length, angle, radius, switch_ }
	}

	lanes_col := track_col["lanes"].([]interface {}) // Lanes here
	value = len(lanes_col)
	var lanes = make([]Lane, value)
	for i := 0; i < value; i++ {
		lanes[i] = Lane {
			lanes_col[i].(map[string]interface {})["distanceFromCenter"].(float64),
			lanes_col[i].(map[string]interface {})["index"].(float64),
		}
	}

	starting_point := StartingPoint {
		track_col["startingPoint"].(map[string]interface {})["position"].(map[string]interface {})["x"].(float64),
		track_col["startingPoint"].(map[string]interface {})["position"].(map[string]interface {})["y"].(float64),
		track_col["startingPoint"].(map[string]interface {})["angle"].(float64),
	}

	ai.track = Track {
		track_col["id"].(string),
		track_col["name"].(string),
		pieces,
		lanes,
		starting_point,
		data["race"].(map[string]interface {})["raceSession"].(map[string]interface {})["laps"].(float64),
		data["race"].(map[string]interface {})["raceSession"].(map[string]interface {})["maxLapTimeMs"].(float64),
		data["race"].(map[string]interface {})["raceSession"].(map[string]interface {})["quickRace"].(bool),
	}

	cars_col := data["race"].(map[string]interface {})["cars"].([]interface {})
	value = len(cars_col)
	var cars = make([]Car, value-1)
	for i := 0; i < value-1; i++ {
		car := Car {
			cars_col[i].(map[string]interface {})["id"].(map[string]interface {})["name"].(string),
			cars_col[i].(map[string]interface {})["id"].(map[string]interface {})["color"].(string),
			cars_col[i].(map[string]interface {})["dimensions"].(map[string]interface {})["length"].(float64),
			cars_col[i].(map[string]interface {})["dimensions"].(map[string]interface {})["width"].(float64),
			cars_col[i].(map[string]interface {})["dimensions"].(map[string]interface {})["guideFlagPosition"].(float64),
		}
		if (ai.is_my_car(car)) {
			ai.my_car = car
		} else {
			cars[i] = car
		}
	}
	ai.rivals = cars

	err = send_ping(writer)
	return
}

func parse_game_start(writer *bufio.Writer, data interface{}) (err error) {
	log.Printf("gameStart")
	err = send_throttle(writer, 1.0)
	return
}

func parse_game_end(writer *bufio.Writer, data interface{}) (err error) {
	log.Printf("gameEnd")
	err = send_ping(writer)
	return
}

func parse_crash(writer *bufio.Writer, data interface{}, tick float64) (err error) {
	log.Printf("crash")
	err = send_ping(writer)
	return
}

func parse_spawn(writer *bufio.Writer, data interface{}, tick float64) (err error) {
	log.Printf("spawn")
	err = send_ping(writer)
	return
}

func parse_dnf(writer *bufio.Writer, data interface{}) (err error) {
	log.Printf("dnf")
	err = send_ping(writer)
	return
}

func parse_lap_finished(writer *bufio.Writer, data map[string]interface{}) (err error) {
	car := Car {
		data[c_car].(map[string]interface {})[c_name].(string),
		data[c_car].(map[string]interface {})[c_color].(string), 0, 0, 0,
	}
	if (ai.is_my_car(car)) {
		lap_time := data[c_lap_time].(map[string]interface{})
		log.Printf("lapFinished with time: %f", lap_time[c_millis].(float64)/1000)
	}
	err = send_ping(writer)
	return
}

func parse_finish(writer *bufio.Writer, data interface{}) (err error) {
	log.Printf("finish")
	err = send_ping(writer)
	return
}

func parse_car_positions(writer *bufio.Writer, data []interface{}, tick float64) (err error) {
	for i := 0; i < len(data); i++ {
		car_position := data[i].(map[string]interface {})
		car := Car {
			car_position[c_id].(map[string]interface {})[c_name].(string),
			car_position[c_id].(map[string]interface {})[c_color].(string), 0, 0, 0,
		}
		position := Position{
			car_position[c_angle].(float64),
			f2i(car_position[c_piece_position].(map[string]interface {})[c_piece_index].(float64)),
			car_position[c_piece_position].(map[string]interface {})[c_piece_distance].(float64),
			f2i(car_position[c_piece_position].(map[string]interface {})[c_lane].(map[string]interface {})[c_start_lane].(float64)),
			f2i(car_position[c_piece_position].(map[string]interface {})[c_lane].(map[string]interface {})[c_end_lane].(float64)),
			car_position[c_piece_position].(map[string]interface {})[c_lap].(float64),
			tick, 0.0, 0.0, false, 0.0, 0.0, 0.0,
		}
		ai.update_car_position(car, position)
	}
	if (ai.should_turbo()) {
		err = send_turbo(writer)
	} else if (ai.should_switch_lane()) {
		err = send_switch_lane(writer, ai.switch_to_left())
	} else {
		err = send_throttle(writer, ai.throttle())
	}
	return
}
