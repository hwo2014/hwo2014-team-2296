package main

import (
	"bufio"
	"fmt"
	"log"
)

const (

)

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, c_ping, make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float64) (err error) {
	err = write_msg(writer, c_throttle, throttle)
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]interface {})
	data[c_bot_id] = make(map[string]string)
	data[c_bot_id].(map[string]string)[c_name] = name
	data[c_bot_id].(map[string]string)[c_key] = key
	data[c_track_name] = c_current_track_name
	data[c_password] = name
	data[c_car_count] = 1
	log.Printf("create_race: %v", data)
	err = write_msg(writer, c_create_race, data)
	return
}

func send_switch_lane(writer *bufio.Writer, left bool) (err error) {
	if (left) {
		err = write_msg(writer, c_switch_lane, c_left)
	} else {
		err = write_msg(writer, c_switch_lane, c_right)
	}
	return
}

func send_turbo(writer *bufio.Writer) (err error) {
	err = write_msg(writer, c_turbo, c_turbo_msg)
	return
}

func extract_data(data interface{}) (msg map[string]interface{}) {
	msg = data.(map[string]interface{})
	return
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}, game_id string, game_tick float64) (err error) {
	switch msgtype {
	case c_join:
		err = parse_join(writer)
	case c_your_car:
		err = parse_your_car(writer, extract_data(data))

	case c_game_init:
		err = parse_game_init(writer, extract_data(data))
	case c_game_start:
		err = parse_game_start(writer, data)
	case c_game_end:
		err = parse_game_end(writer, data)
	case c_crash:
		err = parse_crash(writer, data, game_tick)
	case c_spawn:
		err = parse_spawn(writer, data, game_tick)
	case c_dnf:
		err = parse_dnf(writer, data)

	case c_lap_finished:
		err = parse_lap_finished(writer, extract_data(data))
	case c_finish:
		err = parse_finish(writer, data)

	case c_car_positions:
		err = parse_car_positions(writer, data.([]interface{}), game_tick)
	case c_error:
		log.Printf(fmt.Sprintf("Got error: %v", data))
		log.Printf(game_id)
		send_ping(writer)
	case c_turbo_available:
		err = parse_turbo(writer, extract_data(data))
	default:
		log.Printf("Got msg type: %s", msgtype)
		log.Printf("It's data is: %v", data)
		send_ping(writer)
	}
	return
}

